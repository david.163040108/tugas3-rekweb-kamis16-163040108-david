<?php

require APPPATH . '/libraries/REST_Controller.php';

class Produk extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Produk_model", "produk");
    }

    public function index_get()
    {
        $id = $this->get('id');
        $cari = $this->get('cari');

        if ($cari != "") {
            $produk = $this->produk->cari($cari)->result();
        } else if($id == "") {
            $produk = $this->produk->getData(null)->result();
        } else {
            $produk = $this->produk->getData($id)->result();
        }

        $this->response($produk);
    }

    public function index_put()
    {
            $nama_barang = $this->put('nama_barang');
            $deskripsi = $this->put('deskripsi');
            $harga = $this->put('harga');
            $stok = $this->put('stok');
            $gambar = $this->put('gambar');
            $kategori = $this->put('kategori');
            $id = $this->put('id');
            $data = array(
                'nama_barang' => $nama_barang,
                'deskripsi' => $deskripsi,
                'harga' => $harga,
                'stok' => $stok,
                'gambar' => $gambar,
                'kategori' => $kategori
            );
            $update = $this->produk->update('produk', $data, 'id', $id);

            if ($update) {
                $this->response(array('status' => 'succes', 200));
            } else {
                $this->response(array('status' => 'fail', 502));
            }
    }

    public function index_post()
    {
        $nama_barang = $this->post('nama_barang');
        $deskripsi = $this->post('deskripsi');
        $harga = $this->post('harga');
        $stok = $this->post('stok');
        $gambar = $this->post('gambar');
        $kategori = $this->post('kategori');
        $data = array(
            'nama_barang' => $nama_barang,
            'deskripsi' => $deskripsi,
            'harga' => $harga,
            'stok' => $stok,
            'gambar' => $gambar,
            'kategori' => $kategori
        );
        $insert = $this->produk->insert($data);

        if ($insert) {
            $this->response(array('status' => 'succes', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
      $id = $this->delete('id');
        $delete = $this->produk->delete('produk', 'id', $id);

        if ($delete) {
            $this->response(array('status' => 'succes', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
